# Timber
Timber is a framework to generate logs data. This could be used to test parsers,
show max throughput with a specific log source, or to generate known bad logs.

## Usage

```
Usage:
 timber.py [-n <num> | --number=<num>] [-p | --print] [-f <name> | --file=<name>] [-a | --append] <config>
 timber.py (-h | --help)
 timber.py (--version)

Options:
 -h, --help                 Displays this help message.
 --version                  Shows version information.
 -n <num>, --number=<num>   Number of events to generate.
                            [default: 100]
 -p, --print                Print output instead of writing it to the configured
                            file.
 -f <name>, --file=<name>   File locatin to write output to.
 -a, --append               Flag to append output to provided file. If this flag
                            is not set, the file will be overwritten with no
                            confirmation.
 <config>                   Config file containing the log stream configuration.config

Example:
 ./timber.py -p configs/example.yaml
```

## Example config

This is a silly example configuration that will generate three events, login,
logout, and activeusers. It demonstrates each supported datatype with a common
usage. This yaml file is commented to help explain each section.

```yaml
events:
  # Global timestamp configuration
  timestamp:
    # Timestamp format. Compatible with any strftime variables.
    format: "%Y-%m-%dT%H:%M:%S.%f"
    # How long between simulated events in milliseconds. Program will choose a random value between min and max.
    variance:
      min: 300
      max: 1200
  ids: # List of valid event IDs
    - login
    - logout
    - activeusers
  config:
    # Login event configuration
    login:
      # Format for the login event. This should use mustache format.
      format: "timestamp={{timestamp}},event=login,sourceHostname={{system}},suser={{user}}"
      # All the valid fields. Notice timestamp isn't defined. It is a special field name that will take the global timestamp field
      fields:
        - name: system
          type: string # Field type is string. Choose a random value from the list values below.
          values:
            - MACHINE-TEST1
            - MACHINE-TEST2
            - MACHINE-TEST3
        - name: user
          type: string
          values:
            - admin
            - user1
            - user2
    logout:
      format: "timestamp={{timestamp}},event=logout,suser={{suser}},sessionstart={{sessionstart}}"
      fields:
        # A timestamp field.
        - name: sessionstart
          type: timestamp
          format: "%Y-%m-%dT%H:%M:%S.%f" # Timestamp format
          variance: # Random difference from a range from the source
            min: 20000
            max: 70000

          # Difference source. Timestamp value is from event's current timestamp.
          # Other values are now which would be from the current system time
          # or previous which will keep track of the previous value for this field
          # and add or subtract time from that.
          source: timestamp

          # What direction should the difference be? Past will subtract the variance
          # future will add the variance.
          direction: past
        - name: suser
          type: string
          values:
            - admin
            - user1
            - user2
    activeusers:
      format: "timestamp={{timestamp}},event=activeusers,usercount={{users}}"
      fields:
        # Integer field. Value will be random from between min and max.
        - name: users
          type: integer
          min: 1
          max: 50
```

## Install
```bash
pip install -r requirements.txt
./timber.py --help
```