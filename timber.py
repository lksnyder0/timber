"""
Usage:
 timber.py [-n <num> | --number=<num>] [-p | --print] [-f <name> | --file=<name>] [-a | --append] <config>
 timber.py (-h | --help)
 timber.py (--version)

Options:
 -h, --help                 Displays this help message.
 --version                  Shows version information.
 -n <num>, --number=<num>   Number of events to generate.
                            [default: 100]
 -p, --print                Print output instead of writing it to the configured
                            file.
 -f <name>, --file=<name>   File locatin to write output to.
 -a, --append               Flag to append output to provided file. If this flag
                            is not set, the file will be overwritten with no
                            confirmation.
 <config>                   Config file containing the log stream configuration.config

Example:
 ./timber.py -p configs/example.yaml
"""

import pystache
from datetime import datetime,timedelta
from docopt import docopt
from yaml import load
from random import choice, randrange

args = docopt(__doc__, version='timber version 0.1')

def gen_timestamp(source, minimum, maximum, direction="future"):
    step = randrange(minimum, maximum)
    if direction == "future":
        newtime = source + timedelta(milliseconds=step)
    elif direction == "past":
        newtime = source - timedelta(milliseconds=step)
    return newtime

def main():
    with open(args["<config>"]) as configFile:
        config = load(configFile.read())

    eventIDs = config["events"]["ids"]
    eventsConfig = config["events"]["config"]
    timestampConfig = config["events"]["timestamp"]

    if args["--file"]:
        if args["--append"]:
            flags = "a"
        else:
            flags = "w"
        outFile = open(args["--file"], flags)
    state = dict()
    for i in range(0, int(args["--number"])):
        eventID = choice(eventIDs)
        eventConfig = eventsConfig[eventID]
        eventFormat = eventConfig["format"]
        eventFields = dict()
        previous_timestamp = state.get("timestamp", datetime.now())
        timestamp = gen_timestamp(
            previous_timestamp,
            timestampConfig["variance"]["min"],
            timestampConfig["variance"]["max"]
            )
        eventFields["timestamp"] = timestamp.strftime(timestampConfig["format"])
        for field in eventConfig["fields"]:
            field_type = field["type"]
            field_name = field["name"]
            state_field = "{}.{}".format(eventID,field_name)
            if field_type == "string":
                value = choice(field["values"])
            elif field_type == "timestamp":
                if field.get("update", False):
                    if field["source"] == "previous":
                        source = state.get(state_field, datetime.now())
                    elif field["source"] == "now":
                        source = datetime.now()
                    elif field["source"] == "timestamp":
                        source = timestamp
                    value = gen_timestamp(
                        source,
                        field["variance"]["min"],
                        field["variance"]["max"],
                        field["direction"],
                        field["format"]
                        )
                    state[state_field] = source
                else:
                    value = state.get(state_field, datetime.now().strftime(field["format"]))
            elif field_type == "integer":
                value = randrange(field["min"], field["max"])
            eventFields[field_name] = value
        line = pystache.render(eventFormat, eventFields)
        if args["--print"]:
            print(line)
        if args["--file"]:
            outFile.write("{0}\n".format(line))
        state["timestamp"] = timestamp
    if args["--file"]:
        outFile.close()


main()